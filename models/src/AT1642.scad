A = 4.22;
B = 3.81;
D = 1.27;
E = 0.94;
F = 0.64;
G = 0.30;

lead_height = 0.0381;
base_height = 0.5;
mid_height = 3.152;
top_height = 0.25;

top_sz = 2.54*1.05;

module lead()
    color("red")
        linear_extrude(height=lead_height, scale=1)
            square([G, E], center=true);
            
translate([0*D, 0, 0]) lead();
translate([1*D, 0, 0]) lead();
translate([2*D, 0, 0]) lead();
translate([0*D, A-E, 0]) lead();
translate([2*D, A-E, 0]) lead();

module base()
    color("white")
        linear_extrude(height=base_height, scale=1)
            square([A, B], center=true);
            
module mid()
    color("gray")
        linear_extrude(height=mid_height, scale=1)
            square([top_sz*0.9, top_sz*0.9], center=true);
            
module top()
    color("black")
        linear_extrude(height=top_height, scale=1)
            square([top_sz, top_sz], center=true);
            
translate([D, (A-E)/2, lead_height]) base();
translate([D, (A-E)/2, lead_height+base_height]) mid();
translate([D, (A-E)/2, lead_height+base_height+mid_height]) top();