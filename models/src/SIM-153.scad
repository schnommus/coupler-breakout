A = 5.08;
B = 4.57;
C = 2.21;
E = 1.27;
L = 6.05;
K = 1.52;

module base()
    color("white")
        linear_extrude(height=C*0.4, scale=1)
            square([A, B], center=true);
            
module top()
    color("black")
        linear_extrude(height=C*0.6, scale=1)
            square([A*0.88, B*0.75], center=true);
            
translate([E*1.5, (L-K)/2-K/2, 0]) base();
translate([E*1.5, (L-K)/2-K/2, C*0.4]) top();